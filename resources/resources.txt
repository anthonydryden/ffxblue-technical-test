https://morayodeji.medium.com/building-nodejs-api-without-expressjs-or-any-other-framework-977e8768abb1
- initial set up inspire by the above

https://www.w3schools.com/nodejs/nodejs_url.asp
https://www.w3schools.com/nodejs/nodejs_filesystem.asp
- package information for url and filesystems

https://nodejs.dev/learn/reading-files-with-nodejs
- resource for the fileshare package

https://restfulapi.net/http-status-codes/
- return codes for the request

https://stackoverflow.com/questions/23680121/node-js-with-fs-readfile-not-able-to-render-file-content-via-http
- had an issue with callbacks and the response firing before the readfile

https://regexr.com/
- create regex to filter search results

https://stackoverflow.com/questions/23237704/nodejs-how-to-remove-duplicates-from-array
- filter array in javascript

https://stackoverflow.com/questions/2003815/how-to-remove-element-from-an-array-in-javascript
- remove the first element of the article ID return when above 10 items