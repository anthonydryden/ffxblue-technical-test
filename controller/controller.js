const url = require('url'); // Import url library
const fs = require('fs'); // Import fs library
const dataFile = './data/data.json'; // Specify the location of the data file

exports.addData = function(req, res) {
    body = '';

    // Collect the body of the POST request
    req.on('data', function (chunk) {
        body += chunk;
    });

    // When the POST request ends
    req.on('end', function () {
        try {
            // Parse to a JSON, and then stringify to be able to write it to the file
            postBody = JSON.parse(body);
            stringBody = JSON.stringify(postBody);

            // Response the user receives
            var response = "Articles added successfully."
            
            // Attempt to write to the dataFile specified above
            fs.writeFile(dataFile, stringBody, function (error) {
                if ("Error writing to the data file: " + error) throw error;
            });

            // End request with status code 201: Created
            res.statusCode = 201;
            res.end(response);

        } catch (error) {
            // Catch if bad JSON has been provided
            console.error("Invalid JSON provided: " + error);
            res.statusCode = 400;
            res.end("Invalid JSON provided.");
        };
    });
}

exports.getIdData = function(req, res) {
    // Get and split URL to grab the article ID from it
    const reqUrl = url.parse(req.url, true);
    let splitUrl = reqUrl.pathname.split('/');

    try {
        // Attempt to read the file and convert it to JSON
        var articleData = fs.readFile(dataFile, 'utf8', (error, data) => {
            if (error) {
                console.error("Error with reading file: " + error);
            }
            
            articleData = JSON.parse(data);

            let targetArticleId = splitUrl[2]; // Article ID is the last item in the split
            let matchFound = false; // Start with match not being found so the loop doesn't continue after a result is found

            // Parse through the data file and see if there are any matches
            for (var i = 0; i < articleData.length; i++) {
                try {
                    // If both the requested artdicle ID and the current item ID match and there are no matches found already then store the article data as string
                    if (targetArticleId == articleData[i].id && !matchFound) {
                        let matchedArticle = JSON.stringify(articleData[i], null, 1);

                        //
                        res.statusCode = 200;
                        res.write(matchedArticle, function () {
                            // Callback for the end response as readFile wasn't running otherwise
                            res.end();
                        });
                        matchFound = true;
                    }
                } catch (error) {
                    // Log error with retrieving article if the IF statement failes
                    console.error("Error retriving the requested article: " + error);
                    // Send back failed status code
                    res.statusCode = 400;
                    res.write("Error retriving the requested article.", function () {
                        // Callback for the end response as readFile wasn't running otherwise
                        res.end();
                    });
                }
            }
            if (!matchFound) {
                // Send back failed status code
                res.statusCode = 400;
                res.write("The requested article does not exist.", function () {
                    // Callback for the end response as readFile wasn't running otherwise
                    res.end();
                });
            }
            });
    } catch (error) {
        // If any part of the above application fails, print error with the entire function
        console.error("Error attempting to getIdData: " + error);
    }
    
    
}

exports.getTagData = function(req, res) {
    // Get and split URL to grab the requested tag and date from it
    const reqUrl = url.parse(req.url, true);
    let splitUrl = reqUrl.pathname.split('/');

    try {
        // Grab the data from the data.json file
        var articleData = fs.readFile(dataFile, 'utf8', (error, data) => {
            if (error) {
                console.error("Error with reading file: " + error);
            }
            // Convert to a JSON object
            articleData = JSON.parse(data);
            
            let requestedTag = splitUrl[2]; // Grab the requested tag from the split url
            let requestedDate = splitUrl[3]; // Grab the requested date from the split url

            var articlesMatchingDate = []; // Initialise array to store any articles matching the tag/date request

            for (var i = 0; i < articleData.length; i++) {
                // https://regexr.com/ to help with testing the below regex and change to same date format for comparison
                var formattedArticleDate = articleData[i].date.replace(/-/g,"");
                try {
                    // If an article matches the request in the url, then store it in the matching articles array
                    if (requestedDate == formattedArticleDate && articleData[i].tags.includes(requestedTag)) {
                        articlesMatchingDate.push(articleData[i]);
                    }
                } catch (error) {
                    // Print any errors with pushing articles to the arrayt
                    console.error("Error with finding articles using date: " + error);
                }
            }

            if (articlesMatchingDate.length > 0) {
                var matchingArticleIds = [] // initialise array to hold article IDs that match to print into the output JSON later
                var relatedTags = [] // initialise array to hold related tags to print into the output JSON later

                for (var i = 0; i < articlesMatchingDate.length; i++) {
                    // If there are more than 10 article IDs already stored, drop the first one and add the most recently matched one, otherwise, just add like normal
                    // Assistance from: https://stackoverflow.com/questions/2003815/how-to-remove-element-from-an-array-in-javascript
                    if (matchingArticleIds.length > 10) {
                        matchingArticleIds.splice(0);
                        matchingArticleIds.push(articlesMatchingDate[i].id);
                    } else {
                        matchingArticleIds.push(articlesMatchingDate[i].id);
                    }
                    // Parse through the item and grab the related tags from the tags array in the JSON object
                    for (var j = 0; j < articlesMatchingDate[i].tags.length; j++) {
                        if (articlesMatchingDate[i].tags[j] != requestedTag) {
                            relatedTags.push(articlesMatchingDate[i].tags[j]);
                        }
                    }
                }

                // Filter the tags with assistance from by: https://stackoverflow.com/questions/23237704/nodejs-how-to-remove-duplicates-from-array
                var filteredTags = relatedTags.filter(function (value, index) {
                    return relatedTags.indexOf(value) === index;
                });
                
                // Combine everything into a single response JSON and make it readable
                var tagsResponse = JSON.stringify({
                    "tag" : requestedTag,
                    "count" : articlesMatchingDate.length,
                    "articles" : matchingArticleIds,
                    "related_tags" : filteredTags
                }, null, 1);

                // Success status code and end off response
                res.statusCode = 200;
                res.end(tagsResponse);
            } else {
                // Failed status code as there are no article from that date
                res.statusCode = 400;
                res.end("There are no articles from the supplied date with the supplied tag.")
            }

        });
    } catch (error) {
        console.error("Error attempting to getTagData: " + error);
    }
}

exports.invalidURL = function(req, res) {
    // If the user submits an invalid url, this will catch it and tell them that endpoint isn't available
    res.statusCode = 400; 
    res.end("This endpoint is not available.");
}