const http = require('http'); // Import http library
const url = require('url'); // Import url library
const port = 8080; // The port the server will use
var articleInfo = require('./controller/controller'); // Import the controller file for the functions

// Hosts and listens server on localhost:8080
const server = http.createServer(function (req, res) {
    const reqUrl = url.parse(req.url, true);
    let splitUrl = reqUrl.pathname.split('/'); // Split URL based on the / character

    // splitUrl array starts at [1] due to an empty index at the beginning
    if(splitUrl[1] == "articles" && req.method == "POST") {
        // Add the data to the data.json file, function found in controller.js
        articleInfo.addData(req, res);
    }

    //  https://regexr.com/ used to test the below regex
    else if(splitUrl[1] == "articles" && /\d/.test(splitUrl[2]) && req.method == "GET") {
        // Get the article data based on the ID, function found in controller.js
        articleInfo.getIdData(req, res);
    }

    else if(splitUrl[1] == 'tags' && req.method == 'GET') {
        // Get the requested tag and date, function found in controller.js
        articleInfo.getTagData(req, res);
    }

    else {
        // Endpoint not available, function found in controller.js
        articleInfo.invalidURL(req, res);
    }
});

// Check if there are any errors starting the server and return them
try {
    server.listen(port);
    console.log("Server is listening on port 8080"); // Confirmation message that server is listening
} catch  (error) {
    console.error("There was an issue starting the server on" + port + ": " + error)
}
