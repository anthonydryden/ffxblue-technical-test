# README #

The ffxblue-technical-test application follows the instructions outlined the email for the technical test and installation steps will be documented below. This README will also list any assumptions made, considerations, testing, resources used as well as ways this code can be improved given the research.

## What is this repository for? ##

* This is a RESTful API built by Anthony Dryden for an application to the Junior Software Engineer role.
* v1.0.0

## How do I get set up? ##

* [Summary of set up](#summary-of-set-up)
* [Configuration](#configuration)
* [Dependencies](#dependencies)
* [Database configuration](#database-configuration)
* [How to run tests](#how-to-run-tests)
* [Deployment instructions](#deployment-instructions)

## Design Considerations ##

* [Why did I choose this language/these libraries?](#why-did-i-choose-this-languagethese-libraries)
* [Why is the application built this way?](#why-is-the-application-built-this-way)
* [How are the errors handled?](#how-are-the-errors-handled)
* [How did I test the application?](#how-did-i-test-the-application)
* [What assumptions were made for this application?](#what-assumptions-were-made-for-this-application)
* [Challenges While Testing](#challenges-while-testing)

## Resources Used ##
* [HTTP Requests For Testing](#http-requests-for-testing)
* [URL Resources Used](#url-resources-used)

## Opinions
* [What did I think?](#what-did-i-think)
* [How long did it take to complete?](#how-long-did-it-take-to-complete)
* [What would I have added if I had more time?](#what-would-i-have-added-if-i-had-more-time)

---------------------------------------

## How do I get set up? ###

### Summary of set up
The application itself is hosted in the `app.js` which handles the creation of the HTTP server and begins the listening on port `8080`. The `app.js` application acts as the router for the API and handles and parses the requests, and chooses with function to use based on the requested `URL` and `method`. All functions are in `controller.js` which is located in the `controller` folder. The database is stored in a the `data.json` located in the `data` folder. It's a simple `json` file that is stored locally and stores the `JSON` that is posted.  
The endpoints that are avilable include the below:
- `POST` to `/articles`
- `GET` to `/articles/{id}`
- `GET` to `/tags/{tagName}/{date}`  

This application has been tested on both a Windows machine and a MacOS machine. This repository is cloneable on any device that has GIT installed and can be run with a standard NodeJS installation.

### Configuration
This application uses vanilla JavaScipt and NodeJS. All that is needed is an installation of NodeJS and the ability to run `node` commands from the Terminal or Command Prompt windows. The application uses `HTTP` requests and replied to `POST` and `GET` requests at certain endpoints. The application by default runs on the `localhost` and runs on port `8080`. This can be changed and a new port can be specified in `app.js` under the `port` variable defined at the top.
To be able to use this application, please install NodeJS from the official website: https://nodejs.org/en/    

### Dependencies
This application uses vanilla JavaScript and only built in NodeJS modules. No other `npm` installations or libraries are required.  

### Database configuration
The database consists of the `data.json` file located in the `data` folder.  
`POST` requests to `\articles` that have valid JSON posted in the body are written and committed to the `data.json` file. This file is read whenever needed in the `GET` requests.  

### How to run tests
The application can be run in the Command Prompt or Terminal after the installation of NodeJS.  
The user will need to path to the correct directory and the user can run the `node app.js` command to start the server listening.  
I have opted to use the free installed client of [Postman](https://www.postman.com/) to specify the request method and request body in the testing process.  

---------------------------------------

## Design Considerations ##

### Why did I choose this language/these libraries?
I have the most experience with `Java` and `JavaScript` throughout my time at university and working on independent projects, and JavaScript is an extension of this knowledge. I understand that `NodeJS` is still used in the stack of the business and it has been wonderful to learn more and develop my skills while working on this challenge. I opted to not use any specific extra libraries or modules as I believe it is the best way to learn and full get in touch with the language and learn the quirks. If I am able to code in vanilla JavaScript, then I can fully understand what the libraries provide and how they assist the process and why they are needed.  

### Why is the application built this way?
The beginnings of the application was inspired by [this guide](https://morayodeji.medium.com/building-nodejs-api-without-expressjs-or-any-other-framework-977e8768abb1) which gave me the idea of how to format the file structure and to separate the functions into their own separate controller in `controller.js` and use the `app.js` location for the server listening and routing.

### How are the errors handled?
Functions that have broken during my own testing, such as writing the `data.json` file, reading the `data.json` file or improper reading a file when the response has ended are caught in errors. The errors are formatted with a small written blurb saying what process has failed, and the error is logged with the blurb in the console. The user only sees a general error that the process has failed with a status code stating that the error has failed and why. I used [this website](https://restfulapi.net/http-status-codes/) to assist with getting the status codes.

### How did I test the application?
I have tested the application using the free installed client of [Postman](https://www.postman.com/) and running the server from a `/localhost:8080/` domain. This application has been tested entirely locally and requires NodeJS to be installed on the local machine for testing.  
A valid test JSON and an invalid test JSON can both be found in `.\resources\testing.txt`.  
I used the following URLs for testing this application, and I'll print their success and failures:
- PUSH request to http://localhost:8080/articles : Success with valid JSON in body, failure with invalid JSON in body. JSON used for testing can be found in `.\resources\testing.txt`.  
- GET request to: http://localhost:8080/articles/ : Failure, endpoint is not available as it doesn't handle empty article searches, no ID supplied.
- GET request to: http://localhost:8080/articles/1 : Success, prints details in a pretty JSON for article ID 1.
- GET request to: http://localhost:8080/articles/2 : Success, prints details in a pretty JSON for article ID 2.
- GET request to: http://localhost:8080/articles/3 : Success, prints details in a pretty JSON for article ID 3.
- GET request to: http://localhost:8080/articles/84sb5d : Success, prints details in a pretty JSON for article ID 84sb5d.    

- GET request to: http://localhost:8080/tags/health/20160922 : Success, prints requested tag, number of articles, article IDs and related tags in a pretty JSON format.
- GET request to: http://localhost:8080/tags/house/20160923 : Success, prints requested tag, number of articles, article IDs and related tags in a pretty JSON format.
- GET request to: http://localhost:8080/tags/christmas/20160924 : Success, prints requested tag, number of articles, article IDs and related tags in a pretty JSON format.
- GET request to: http://localhost:8080/tags/christmas/20160928 : Failure, prints that there are no requested articles for that date with that tag.

### What assumptions were made for this application?
I have made the following assumptions with the program:
- An `article` must always have at least 1 tag associated with it.
- An `article` must always have the tags in an array value.
- The `article.date` format must always come in the format `yyyy-mm-dd`.

### Challenges While Testing
I wanted to write a section about some errors and issues I was receiving while testing and how I resolved them:
- `Error with finding matched article: TypeError: Cannot read properties of undefined (reading 'id')`
    * Getting this for a long time as the for loop was set to be <= instead of <, and was exceeding the size of the JSON array. I was caught by the array starting at index 0 and the first ID for the article being 1, so I thought there was a possible mismatch.
- `Loop was still running after a match was found`
    * Added the match found variable
- `Sometimes res.end and fs.readFile were not firing at the same time`
    * Due to NodeJS being an asynchronous language, the readFile wasn't occuring before the request, as the response happens first.[This link](https://stackoverflow.com/questions/23680121/node-js-with-fs-readfile-not-able-to-render-file-content-via-http) helped me to troubleshoot the issue, and readFile() now calls the end of the response with a callback.


---------------------------------------

## Resources Used ##

### HTTP Requests For Testing
I used the desktop installation of [Postman](https://www.postman.com/) to test the following HTTP requests to the \localhost:8080\ location:  
- PUSH request to http://localhost:8080/articles
- GET request to: http://localhost:8080/articles/
- GET request to: http://localhost:8080/articles/1
- GET request to: http://localhost:8080/articles/2
- GET request to: http://localhost:8080/articles/3
- GET request to: http://localhost:8080/articles/84sb5d   
- GET request to: http://localhost:8080/tags/health/20160922 
- GET request to: http://localhost:8080/tags/house/20160923
- GET request to: http://localhost:8080/tags/christmas/20160924
- GET request to: http://localhost:8080/tags/christmas/20160928

### URL Resources Used
I used the following URL resouces and guides to assist with my code:  
- A guide on how to initially set up the project:
    * https://morayodeji.medium.com/building-nodejs-api-without-expressjs-or-any-other-framework-977e8768abb1
- W3Schools walkthroughs for the URL and Filesystem modules:
    * https://www.w3schools.com/nodejs/nodejs_url.asp
    * https://www.w3schools.com/nodejs/nodejs_filesystem.asp
- Offical NodeJS website for reading files and catching errors with files:
    * https://nodejs.dev/learn/reading-files-with-nodejs
- List of all returnable status codes:
    * https://restfulapi.net/http-status-codes/
- Stackoverflow post to resolve an issue I was having with callback functions:
    * https://stackoverflow.com/questions/23680121/node-js-with-fs-readfile-not-able-to-render-file-content-via-http
- Regex is used to check if number are correct and this has assisted in creating the correct regex:
    * https://regexr.com/
- Stackoverflow post on how to remove duplicates from the related tags lists in NodeJS, used in `controller.js` for `getTagData()`:
    * https://stackoverflow.com/questions/23237704/nodejs-how-to-remove-duplicates-from-array
- Stackoverflow post on how to limit an array to 10 items and remove the first element, used in `controller.js` for `getTagData()` :
    * https://stackoverflow.com/questions/2003815/how-to-remove-element-from-an-array-in-javascript

---------------------------------------

## Opinions
### What did I think?
I thought this challenge was amazing fun! It was great learning and getting deep into building JSON and working with arrays. It has been a while since I've had a problem that has needed a nested loop as well! Thank you again for this opportunity and I look forward to hearig from you!

### How long did it take to complete?
I was giving the coding challenge of Friday evening which I worked late, and had to work on Saturday as well. All-in-all, everything took around 10 hours, including this documentation and README file. I would have loved to spend the whole weekend working on the project if I didn't work Saturday, and have a prior commitment on Monday day.

### What would I have added if I had more time?
If I had more time, I would like to do the following:
- Rebuild the `app.js` to use the `Express` NodeJS library. In my research, it seems to be an incredibly powerful library and handles and simplifies HTTP requests. 
- Go through and allow users to put in whatever URL they would like to use for the server hosting, as currently it sits on `\localhost\`.
- Add in the `MongoDB` and `Mongoose` librarys to add in a fully fledged database that handles the JSON objects as opposed to having a simple `data.json` file.
- Find a more efficient way to split the URL at the `/` character.

